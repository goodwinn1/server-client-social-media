package serverside;

import java.util.ArrayList;
import javax.swing.*;
/**
 *
 * @author Nicholas
 */
public class User {
    boolean Online;
    String username = "";
    String password = "asdf";
    String name = "";
    String DoB = "";
    ArrayList<String> followers = new ArrayList<String>();
    ArrayList<String> followees = new ArrayList<String>();
    ArrayList<Message> publicMessages = new ArrayList<Message>();
    
    public User(String user, String pass, String Name, String Date) {
        username = user;
        password = pass;
        name = Name;
        DoB = Date;
        followers.add(user);
        followees.add(user);
    }
    
    public String getPassword() {
        return password;
    }
    
    public void addFollower(String username) {
        boolean alreadyIn = followers.contains(username);
        if (alreadyIn) {
            
        } else {
            followers.add(username);
        }
    }
    
    public boolean addFollowee(String username) {
        boolean alreadyIn = followees.contains(username);
        if (alreadyIn) {
            return false;
        } else {
            followees.add(username);
            return true;
        }
    }
    
    //People who follow me
    public String[] getFollowers() {
        String[] follow = new String[followers.size()];
        int count = 0;
        for(String follower : followers) {
            follow[count] = follower;
            count++;
        }
        System.out.println(follow);
        return follow;
    }
    
    //People I follow
    public ArrayList<String> getFollowees() {
        return followees;
    }
    
    public String getName() {
        return name;
    }
    
    public void LogIn() {
        Online = true;
    }
    
    public void LogOff() {
        Online = false;
    }
    
    public void ToString() {
        System.out.println("User:\n" + username + "\n" + name);
    }
    
    public void NewMessage(Message message) {
        publicMessages.add(message);
    }
    
    public String getOnlineStatus() {
        if (Online) {
            return "Online";
        } else {
            return "Offline";
        }
    }
    
    public String[] getMessages() {
        String[] Messages = new String[publicMessages.size()];
        for (int i = 0; i < publicMessages.size(); i++) {
            Message temp = publicMessages.get(i);
            String moreTemp = temp.ToString();
            Messages[i] = moreTemp;
        }
        return Messages;
    }
}


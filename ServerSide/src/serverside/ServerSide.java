/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverside;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.UnknownHostException;
/**
 *
 * @author Nicholas
 */
public class ServerSide {
    
    UserCollection collection = new UserCollection();
    MessageCollection Mcollection = new MessageCollection();
    
    public static void main(String [] args) {
        ServerSide server = new ServerSide();
    }
    
    public ServerSide() {
        try {
            System.out.println("Server instantiated");
            ServerSocket sock = new ServerSocket(2005);
            while(true) {
                Socket client = sock.accept();
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter out = new PrintWriter(client.getOutputStream(), true);
                System.out.println("Server waiting for response");
                String inputLine = in.readLine() + "";
                
                if (inputLine.startsWith("FINDUSER")) {
                    System.out.println("Finding User");
                    String user = inputLine.replace("FINDUSER ", "");
                    boolean b = FindUser(user);
                    if (b) {
                        out.println("Successful");
                    } else {
                        out.println("Failure");
                    }
                }
                if (inputLine.startsWith("ADDUSER")) {
                    System.out.println("Adding User");
                    String temp = inputLine.replace("ADDUSER ", "");
                    int userIndex = temp.indexOf(" ");
                    String username = temp.substring(0, userIndex + 1);
                    temp = temp.replaceFirst(username, "");
                    userIndex = temp.indexOf(" ");
                    String password = temp.substring(0, userIndex + 1);
                    temp = temp.replace(password, "");
                    userIndex = temp.indexOf(" ");
                    String name = temp.substring(0, userIndex + 1);
                    temp = temp.replace(name, "");
                    System.out.println(username + "\n" + password + "\n" + name + "\n" + temp);
                    boolean b = addUser(username.trim(), password.trim(), name.trim(), temp.trim());
                    if (b) {
                        out.println("Successful");
                    } else {
                        out.println("Failure");
                    }
                }
                if (inputLine.startsWith("LOGIN")) {
                    System.out.println("Attempting to log in");
                    String temp = inputLine.replace("LOGIN ", "");
                    int userIndex = temp.indexOf(" ");
                    String username = temp.substring(0, userIndex);
                    String password = temp.replace(username + " ", "");
                    boolean b = this.Login(username.trim(), password.trim());
                    if (b) {
                        System.out.println("Login Successful");
                        out.println("Successful");
                    } else {
                        out.println("Failure");
                    }
                }
                if (inputLine.startsWith("FOLLOW")) {
                    System.out.println("Following Someone");
                    String temp = inputLine.replace("FOLLOW ", "");
                    int userIndex = temp.indexOf(" ");
                    String myUsername = temp.substring(0, userIndex);
                    String username = temp.substring(userIndex + 1);
                    System.out.println("Following: " + username);
                    System.out.println("I am: " + myUsername);
                    boolean b = FollowUser(myUsername.trim(), username.trim());
                    if (b) {
                        out.println("Successful");
                    } else {
                        out.println("Failure");
                    }
                }
                if (inputLine.startsWith("LOGOFF")) {
                    String temp = inputLine.replace("LOGOFF ", "");
                    System.out.println("Logging off: " + temp);
                    LogOff(temp.trim());
                    out.println("Successful");
                }
                if (inputLine.startsWith("GETFOLLOWEES")) {
                    System.out.println("Getting Followees");
                    String friends = "";
                    String temp = inputLine.replace("GETFOLLOWEES ", "");
                    ArrayList<String> followees = collection.getFollowees(temp);
                    for (int i = 0; i < followees.size(); i++) {
                        friends = friends + followees.get(i) + " ";
                    }
                    out.println(friends);
                }
                if (inputLine.startsWith("SENDMESSAGE ")) {
                    String temp = inputLine.replace("SENDMESSAGE ", "");
                    int index = temp.indexOf(" ");
                    String username = temp.substring(0, index).trim();
                    temp = temp.replace(username + " ", "");
                    index = temp.indexOf(" ");
                    String topic = temp.substring(0, index).trim();
                    String body = temp.replace(topic + " ", "");
                    System.out.println("Sender: " + username);
                    System.out.println("Sending message with topic: " + topic);
                    System.out.println(body);
                    Message message = new Message(topic, body, username);
                    String Msuccessful = Mcollection.NewMessage(message);
                    String Usuccessful = collection.NewMessage(username, message);
                    if (Msuccessful.equals("successful") && Usuccessful.equals("successful")) {
                        out.println("successful");
                    } else {
                        out.println("failure");
                    }
                }
                if (inputLine.startsWith("GETONLINEFRIENDS")) {
                    System.out.println("Getting online friends");
                    String temp = inputLine.replace("GETONLINEFRIENDS ", "").trim();
                    String outgoing = "";
                    ArrayList<String> Online = collection.getOnlineFriends(temp);
                    for (int i = 0; i < Online.size(); i++) {
                        outgoing = outgoing + " " + Online.get(i);
                    }
                    out.println(outgoing);
                }
                if (inputLine.startsWith("GETOFFLINEFRIENDS")) {
                    System.out.println("Getting offline friends");
                    String temp = inputLine.replace("GETOFFLINEFRIENDS ", "").trim();
                    String outgoing = "";
                    ArrayList<String> Offline = collection.getOfflineFriends(temp);
                    for (int i = 0; i < Offline.size(); i++) {
                        outgoing = outgoing + " New Message:";
                    }
                    out.println(outgoing);
                }
                if (inputLine.startsWith("UPDATEMESSAGES")) {
                    System.out.println("Getting messages");
                    String temp = inputLine.replace("UPDATEMESSAGES ", "");
                    String outgoing = "";
                    ArrayList<String> Messenging = collection.getMessages(temp);
                    for (int i = 0; i < Messenging.size(); i++) {
                        System.out.println(outgoing);
                        outgoing = outgoing + "~" + Messenging.get(i);
                    }
                    out.println(outgoing);
                }
            }
            
        } catch (UnknownHostException e) {
            System.err.println("Add Protocol: no such host");
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
    }
    
    public boolean FindUser(String user) {
        boolean b = collection.FindUser(user);
        if (b == false) {
            return false;
        } else {
            return true;
        }
    }
    
    public boolean addUser(String username, String password, String name, String DoB) {
        User user = new User(username, password, name, DoB);
        boolean b = collection.addUser(username, user);
        return b;
    }
    
    public boolean Login(String username, String password) {
        String login = collection.Authentication(username, password);
        boolean b;
        if (login.equals("Successful")) {
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean FollowUser(String myUsername, String username) {
        boolean successful = false;
        
        User followee = collection.getUser(myUsername);
        User follower = collection.getUser(username);
        followee.addFollower(username);
        successful = follower.addFollowee(myUsername);
        
        return successful;
    }
    
    public boolean LogOff(String username) {
        User user = collection.getUser(username);
        user.LogOff();
        return true;
    }
}
package serverside;
import java.util.Date;



public class Message {
    String Topic;
    String Body;
    String user;
    Date date = new java.util.Date();
    
    public Message(String topic, String body, String username) {
        Topic = topic;
        Body = body;
        user = username;
    }
    
    public String getTopic() {
        return Topic;
    }
    
    public String ToString() {
        String full = date.toString();
        full = full + "  " + user + "  " + Topic + "  " + Body;
        return full;
    }
}

package serverside;

import java.util.HashMap;
import java.util.ArrayList;
/**
 *
 * @author Nicholas
 */
public class UserCollection {
    
    HashMap<String, User> users = new HashMap<String, User>();
    
    public UserCollection() {
        User Tim = new User("Tim", "asdf", "Tim", "01/12/1996");
        users.put("Tim", Tim);
    }
    
    public boolean FindUser(String username) {
        System.out.println("User Collection: Finding user: " + username);
        boolean validUser = users.containsKey(username);
        return validUser;
    }
    
    public boolean addUser(String username, User user) {
        users.put(username, user);
        return true;
    }
    
    public User getUser(String username) {
        return users.get(username);
    }
    
    public String Authentication(String username, String password) {
        if (!username.equals("") && !password.equals("")) {
            User temp = users.get(username);
            String pass = temp.getPassword();
            if (!temp.getOnlineStatus().equals("Online")) {
            if (password.equals(pass)) {
                temp.LogIn();
                return "Successful";
            } else {
                return "Failed";
            }
            } else {
                return "Failed";
            }
        } else {
            return "Failed";
        }
    }
    
    public ArrayList<String> getFollowees(String username) {
        User temp = getUser(username);
        ArrayList<String> followees = temp.getFollowees();
        return followees;
    }
    
    public String NewMessage(String username, Message message) {
        User sender = users.get(username);
        sender.NewMessage(message);
        ArrayList<String> followees = sender.getFollowees();
        for (int i = 0; i < followees.size(); i++) {
            String tempUser = followees.get(i);
            User temp = users.get(tempUser);
            temp.NewMessage(message);
        }
        return "successful";
    }
    
    public ArrayList<String> getOnlineFriends(String user) {
        System.out.println("UserCollection: Getting Online Friends");
        ArrayList<String> OnlineFriends = new ArrayList<String>();
        User myUser = users.get(user);
        String[] Followees = myUser.getFollowers();
        for (int i = 0; i < Followees.length; i++) {
            String temp = Followees[i];
            User userTemp = users.get(temp);
            String Status = userTemp.getOnlineStatus();
            if (Status.equals("Online")) {
                OnlineFriends.add(temp);
            }
        }
        return OnlineFriends;
    }
    
    public ArrayList<String> getOfflineFriends(String user) {
        System.out.println("userCollection: Getting Offline Friends");
        ArrayList<String> OfflineFriends = new ArrayList<String>();
        User myUser = users.get(user);
        ArrayList<String> Followees;
        if (myUser.getFollowees() != null) {
            Followees = myUser.getFollowees();
            for (int i = 0; i < Followees.size(); i++) {
                String temp = Followees.get(i);
                User userTemp = users.get(temp);
                String Status = userTemp.getOnlineStatus();
                if (Status.equals("Offline")) {
                    OfflineFriends.add(temp);
                }
            }
        }
        return OfflineFriends;
    }
    
    public ArrayList<String> getMessages(String user) {
        System.out.println("UserCollection: Getting awaited messages");
        ArrayList<String> Messages = new ArrayList<String>();
        User myUser = users.get(user);
        String[] allMessages = myUser.getMessages();
        for (int i = 0; i < allMessages.length; i++) {
            String temp = allMessages[i];
            Messages.add(temp);
        }
        return Messages;
    }
}

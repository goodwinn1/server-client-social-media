package serverside;

import java.util.HashMap;
import java.util.ArrayList;

public class MessageCollection {
    
    HashMap<String, ArrayList> messages = new HashMap<String, ArrayList>();
    
    public MessageCollection() {
        
    }
    
    public String NewMessage(Message message) {
        String topic = message.getTopic();
        if (messages.containsKey(topic)) {
            ArrayList temp = messages.get(topic);
            temp.add(message);
        } else {
            ArrayList Topic = new ArrayList();
            Topic.add(message);
            messages.put(topic, Topic);
        }
        return "successful";
    }
}

package clientside;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JLabel;
import java.util.ArrayList;
import java.util.HashMap;

public class MainInterface extends JFrame {
    
    String username;
    Engine engine;
    
    public MainInterface(String username, Engine engine) {
        this.username = username;
        this.engine = engine;
        createUserInterface();
    }
    
    private JLabel Username;
    private JList PublicMessages;
    private JList OnlineFriends;
    private JLabel publicMessages;
    private JLabel friends;
    private JButton PrivateMessages;
    private JButton makePublic;
    private JButton makePrivate;
    private JButton FollowSomeone;
    private JButton LogOff;
    private JButton Refresh;
    public static DefaultListModel Friends;
    public static DefaultListModel Message;
    
    private void createUserInterface() {
        setVisible(true);
        this.addWindowListener(new WindowAdapter() {
            public void WindowClosing(WindowEvent e) {
                engine.LogOff(username);
            }
        });
        Container contentPane = getContentPane();
        
        contentPane.setLayout(null);
        setTitle("Totally Original Social Media");
        setSize(800, 500);
        contentPane.setBackground(Color.red);
        
        Username = new JLabel();
        Username.setBounds(10, 10, 150, 40);
        Username.setText(username);
        contentPane.add(Username);
        Username.setFont(new Font("Serif", Font.PLAIN, 40));
        
        Message = new DefaultListModel();
        
        PublicMessages = new JList(Message);
        PublicMessages.setBounds(10, 100, 150, 300);
        contentPane.add(PublicMessages);
        
        Friends = new DefaultListModel();
        
        OnlineFriends = new JList(Friends);
        OnlineFriends.setBounds(500, 100, 150, 300);
        contentPane.add(OnlineFriends);
        
        publicMessages = new JLabel();
        publicMessages.setBounds(10, 80, 100, 20);
        publicMessages.setText("Public Messages");
        contentPane.add(publicMessages);
        
        friends = new JLabel();
        friends.setBounds(500, 80, 150, 20);
        friends.setText("Online/Offline Friends");
        contentPane.add(friends);
        
        PrivateMessages = new JButton();
        PrivateMessages.setBounds(200, 100, 200, 20);
        PrivateMessages.setText("View Private Messages");
        contentPane.add(PrivateMessages);
        
        makePublic = new JButton();
        makePublic.setBounds(200, 185, 200, 20);
        makePublic.setText("Make a Public Message");
        contentPane.add(makePublic);
        makePublic.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PublicMessage publicSend = new PublicMessage(username, engine);
            }
        });
        
        makePrivate = new JButton();
        makePrivate.setBounds(200, 250, 200, 20);
        makePrivate.setText("Make a Private Message");
        contentPane.add(makePrivate);
        
        FollowSomeone = new JButton();
        FollowSomeone.setBounds(200, 300, 200, 20);
        FollowSomeone.setText("Follow another user");
        contentPane.add(FollowSomeone);
        FollowSomeone.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FollowSomeoneGUI frm = new FollowSomeoneGUI(engine, username);
            }
        });
        
        Refresh = new JButton();
        Refresh.setBounds(200, 350, 200, 20);
        Refresh.setText("Refresh");
        contentPane.add(Refresh);
        Refresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                HashMap<String, ArrayList<String>> temp = engine.refresh(username);
                Update(temp);
            }
        });
        LogOff = new JButton();
        LogOff.setBounds(700, 10, 80, 20);
        LogOff.setText("Log off");
        contentPane.add(LogOff);
        LogOff.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ClientSide frm = new ClientSide();
                setVisible(false);
            }
        });
    }
    
    public void Update(HashMap<String, ArrayList<String>> update) {
        Friends.removeAllElements();
        Message.removeAllElements();
        ArrayList<String> Online = update.get("Online Friends");
        updateOnlineFriends(Online);
        ArrayList<String> Offline = update.get("Offline Friends");
        updateOfflineFriends(Offline);
        ArrayList<String> Messages = update.get("New Messages");
        updateMessages(Messages);
        repaint();
    }
    
    public void updateOnlineFriends(ArrayList<String> friends) {
        Friends.addElement("Online Friends:*****");
        for (int i = 0; i < friends.size(); i++) {
            Friends.addElement(friends.get(i));
        }
    }
    
    public void updateOfflineFriends(ArrayList<String> friends) {
        Friends.addElement(" ");
        Friends.addElement("Offline Friends:*****");
        for (int i = 0; i < friends.size(); i++) {
            Friends.addElement(friends.get(i));
        }
    }
    
    public void updateMessages(ArrayList<String> messages) {
        for (int i = 0; i < messages.size(); i++) {
            String temp = messages.get(i);
            String time = temp.substring(0, temp.indexOf("2015  "));
            temp = temp.replace(time, "");
            System.out.println(time);
            Message.addElement(time);
            
        }
    }
}
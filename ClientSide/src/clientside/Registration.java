package clientside;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Arrays;
/**
 *
 * @author Nicholas
 */
public class Registration extends JFrame{
    
    private JLabel User;
    private JTextField Username;
    private JLabel Pass;
    private JPasswordField Password;
    private JLabel ConfirmPass;
    private JPasswordField ConfirmPassword;
    private JLabel name;
    private JTextField Name;
    private JLabel DoB;
    private JTextField DateOfBirth;
    private JButton OK;
    private JButton Cancel;
    
    public Registration(Engine engine) {
        Container contentPane = getContentPane();
        
        contentPane.setLayout(null);
        
        User = new JLabel();
        User.setBounds(10, 10, 100, 10);
        User.setText("Your Username:");
        contentPane.add(User);
        
        Username = new JTextField();
        Username.setBounds(10, 20, 200, 20);
        Username.setText("Susan");
        contentPane.add(Username);
        
        Pass = new JLabel();
        Pass.setBounds(10, 50, 100, 10);
        Pass.setText("Your Password:");
        contentPane.add(Pass);
        
        Password = new JPasswordField();
        Password.setBounds(10, 60, 150, 20);
        contentPane.add(Password);
        
        ConfirmPass = new JLabel();
        ConfirmPass.setBounds(10, 90, 150, 10);
        ConfirmPass.setText("Confirm your password:");
        contentPane.add(ConfirmPass);
        
        ConfirmPassword = new JPasswordField();
        ConfirmPassword.setBounds(10, 100, 150, 20);
        contentPane.add(ConfirmPassword);
        
        name = new JLabel();
        name.setBounds(10, 130, 100, 10);
        name.setText("Your name:");
        contentPane.add(name);
        
        Name = new JTextField();
        Name.setBounds(10, 140, 200, 20);
        Name.setText("Susan");
        contentPane.add(Name);
        
        DoB = new JLabel();
        DoB.setBounds(10, 170, 150, 10);
        DoB.setText("Your Date of Birth:");
        contentPane.add(DoB);
        
        DateOfBirth = new JTextField();
        DateOfBirth.setBounds(10, 180, 100, 20);
        DateOfBirth.setText("mm/dd/yyyy");
        contentPane.add(DateOfBirth);
        
        OK = new JButton();
        OK.setBounds(10, 210, 100, 20);
        OK.setText("OK");
        contentPane.add(OK);
        OK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                char[] password = Password.getPassword();
                String PASSWORD = "";
                for (int i = 0; i < password.length; i++) {
                    PASSWORD += password[i];
                }
                if (!Username.getText().equals("") && !PASSWORD.equals("") && !Name.getText().equals("") && !DateOfBirth.getText().equals("")) {
                    engine.addUser(Username.getText(), password, Name.getText(), DateOfBirth.getText());
                    setVisible(false);
                    MainInterface mainGUI = new MainInterface(Username.getText(), engine);
                } else {
                    JOptionPane.showMessageDialog(null, "Please enter in all fields");
                }
            }
        });
        
        Cancel = new JButton();
        Cancel.setBounds(10, 230, 100, 20);
        Cancel.setText("Cancel");
        contentPane.add(Cancel);
        Cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        setTitle("Registration");
        setSize(250, 350);
        setVisible(true);
    }
}


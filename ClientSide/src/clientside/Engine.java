package clientside;

import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import java.net.Socket;

/**
 *
 * @author Nicholas
 */
public class Engine {
    
    
    public Engine() {
        
    }
    
    public boolean Authenticate(String username, String password) {
        boolean successful = false;
        try {
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String out = "";
            try {
                output.println("LOGIN " + username + " " + password);
                out = input.readLine();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                System.err.println(e.getMessage());
            }
            if (out.equals("Successful")) {
                successful = true;
            } else {
                JOptionPane.showMessageDialog(null, "Invalid Login");
            }
            s.close();
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        return successful;
    }
    
    public boolean addUser(String username, char[] password, String name, String DoB) {
        boolean successful = false;
        try {
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("FINDUSER " + username);
            String Password = "";
            for (int i = 0; i < password.length; i++) {
                Password = Password + password[i];
            }
            try {
                String UserFound = input.readLine();
                s.close();
                if (UserFound.equals("Failure")) {
                    Socket t = new Socket("10.21.32.250", 2005);
                    PrintWriter toutput = new PrintWriter(t.getOutputStream(), true);
                    BufferedReader tinput = new BufferedReader(new InputStreamReader(t.getInputStream()));
                    toutput.println("ADDUSER " + username + " " + Password + " " + name + " " + DoB);
                    String SuccessfulUser = tinput.readLine();
                    if (SuccessfulUser.equals("Successful")) {
                        successful = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "Failure to make new User");
                    }
                    t.close();
                } else {
                    JOptionPane.showMessageDialog(null, "User not Found");
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                System.err.println(e.getMessage());
            }
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        return successful;
    }
    
    public boolean FollowUser(String myUsername, String username) {
        boolean successful = false;
        try {
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("FINDUSER " + username);
            try {
                String UserFound = input.readLine();
                s.close();
                System.out.println(UserFound);
                if (UserFound.equals("Successful")) {
                    Socket t = new Socket("10.21.32.250", 2005);
                    PrintWriter toutput = new PrintWriter(t.getOutputStream(), true);
                    BufferedReader tinput = new BufferedReader(new InputStreamReader(t.getInputStream()));
                    toutput.println("FOLLOW " + myUsername + " " + username);
                    String FollowedSomeone = tinput.readLine();
                    if (FollowedSomeone.equals("Successful")) {
                        JOptionPane.showMessageDialog(null, "Successfully following " + username);
                        successful = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "Failed to follow user");
                    }
                    t.close();
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to Find User");
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                System.err.println(e.getMessage());
            }
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        return successful;
    }
    
    public void LogOff(String myUsername) {
        try {
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            boolean b = false;
            output.println("LOGOFF " + myUsername);
            try {
                String SuccessfulLogOff = input.readLine();
                if (SuccessfulLogOff.equals("Successful")) {
                    JOptionPane.showMessageDialog(null, "Successfully Logged off\n\n Goodbye");
                    System.exit(0);
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to Log off");
                }
                s.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                System.err.println(e.getMessage());
            }
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
    }
    
    public ArrayList<String> getFollowees(String username) {
        ArrayList<String> Followees = new ArrayList<String>();
        try {
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("GETFOLLOWEES " + username);
            String followees = input.readLine();
            while (followees.contains(" ")) {
                int x = followees.indexOf(" ");
                String temp = followees.substring(0, x+1);
                Followees.add(temp);
                followees = followees.replace(temp, "");
            }
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        return Followees;
    }
    
    public void SendPublicMessage(String username, String topic, String body) {
        try {
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("SENDMESSAGE " + username + " " + topic + " " + body);
            String successful = input.readLine();
            if (successful.equals("successful")) {
                JOptionPane.showMessageDialog(null, "You have successfully send the message");
            } else {
                JOptionPane.showMessageDialog(null, "There was an error sending this message, please try again.");
            }
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
    }
    
    public HashMap<String, ArrayList<String>> refresh(String username) {
        HashMap<String, ArrayList<String>> fullUpdate = new HashMap<String, ArrayList<String>>();
        try {
            ArrayList<String> Friends = new ArrayList<String>();
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("GETONLINEFRIENDS " + username);
            String OnlineFriends = input.readLine();
            System.out.println(OnlineFriends);
            while (OnlineFriends.contains(" ")) {
                int index = OnlineFriends.indexOf(" ");
                String temp = OnlineFriends.substring(0, index);
                System.out.println(temp);
                OnlineFriends = OnlineFriends.replace(temp + " ", "").trim();
                Friends.add(temp);
            }
            fullUpdate.put("Online Friends", Friends);
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        try {
            ArrayList<String> Friends = new ArrayList<String>();
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("GETOFFLINEFRIENDS " + username);
            String OfflineFriends = input.readLine();
            System.out.println(OfflineFriends);
            while (OfflineFriends.contains(" ")) {
                int index = OfflineFriends.indexOf(" ");
                String temp = OfflineFriends.substring(0, index);
                System.out.println(temp);
                OfflineFriends = OfflineFriends.replace(temp + " ", "").trim();
                Friends.add(temp);
            }
            fullUpdate.put("Offline Friends", Friends);
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        try {
            ArrayList<String> Messages = new ArrayList<String>();
            Socket s = new Socket("10.21.32.250", 2005);
            PrintWriter output = new PrintWriter(s.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            output.println("UPDATEMESSAGES " + username);
            String Message = input.readLine();
            while (Message.contains("~")) {
                int index = Message.indexOf("~");
                String temp = Message.substring(0, index).trim();
                System.out.println(temp);
                Message = Message.replace(temp + "~", "");
                Messages.add(temp);
            }
            Messages.add(Message.trim());
            fullUpdate.put("New Messages", Messages);
        } catch (IOException e) {
            System.err.println("IOEXCEPTION");
            System.err.println(e.getMessage());
        }
        return fullUpdate;
    }
}

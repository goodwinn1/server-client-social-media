package clientside;
import java.awt.*;
import javax.swing.*;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PublicMessage extends JFrame {
    
    String username;
    Engine engine;
    
    public PublicMessage(String user, Engine engine) {
        this.username = user;
        this.engine = engine;
        createUserInterface();
    }
    
    private JLabel topic;
    private JTextField topicText;
    private JLabel body;
    private JTextArea bodyText;
    private JButton Send;
    private JButton Cancel;
    
    private void createUserInterface() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        setTitle("Public Message");
        setSize(500, 400);
        
        Cancel = new JButton();
        Cancel.setBounds(90, 200, 75, 20);
        Cancel.setText("Cancel");
        contentPane.add(Cancel);
        Cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        
        Send = new JButton();
        Send.setBounds(10, 200, 75, 20);
        Send.setText("Send");
        contentPane.add(Send);
        Send.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!topicText.getText().contains(" ")) {
                    if (!bodyText.getText().equals("") && !topicText.getText().equals("")) {
                        engine.SendPublicMessage(username, topicText.getText(), bodyText.getText());
                        setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(null, "Please enter a topic and body");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Make sure your topic has no spaces");
                }
            }
        });
        
        bodyText = new JTextArea("Body here", 5, 40);
        bodyText.setBounds(10, 80, 400, 100);
        bodyText.setLineWrap(true);
        contentPane.add(bodyText);
        
        body = new JLabel();
        body.setBounds(10, 60, 200, 20);
        body.setText("Please enter the text:");
        contentPane.add(body);
        
        topicText = new JTextField();
        topicText.setBounds(10, 30, 100, 20);
        contentPane.add(topicText);
        
        topic = new JLabel();
        topic.setBounds(10, 10, 200, 20);
        topic.setText("Please enter a topic:");
        contentPane.add(topic);
        
        setVisible(true);
    }
}

package clientside;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author Nicholas
 */
public class FollowSomeoneGUI extends JFrame {
    
    JLabel userLabel;
    JTextField userFind;
    JButton Find;
    Engine engine;
    
    public FollowSomeoneGUI(Engine engine, String username) {
        this.engine = engine;
        
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        
        
        userLabel = new JLabel();
        userLabel.setBounds(10, 10, 200, 10);
        userLabel.setText("Who is it you want to follow?");
        contentPane.add(userLabel);
        
        userFind = new JTextField();
        userFind.setBounds(10, 20, 150, 20);
        userFind.setText("Tim");
        contentPane.add(userFind);
        
        Find = new JButton();
        Find.setBounds(10, 40, 100, 20);
        Find.setText("Follow User");
        contentPane.add(Find);
        Find.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!userFind.getText().equals("")) {
                    if (engine.FollowUser(username, userFind.getText())) {
                        setVisible(false);
                    } else {
                        
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "please enter a username to follow");
                }
            }
        });
        
        setTitle("Follow Someone");
        setSize(200, 100);
        setVisible(true);
    }
}
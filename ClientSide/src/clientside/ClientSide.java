
package clientside;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Nicholas
 */
public class ClientSide extends JFrame {
    
    Engine engine = new Engine();
    private JLabel User;
    private JTextField Username;
    private JLabel Pass;
    private JPasswordField Password;
    private JButton OK;
    private JButton Cancel;
    private JButton Registration;

    public ClientSide() {
        createUserInterface();
    }
    
    private void createUserInterface() {
        Container contentPane = getContentPane();
        
        contentPane.setLayout(null);
        
        User = new JLabel();
        User.setBounds(10, 10, 100, 10);
        User.setText("Username:");
        contentPane.add(User);
        
        Username = new JTextField();
        Username.setBounds(10, 30, 150, 20);
        Username.setText("Susan");
        contentPane.add(Username);
        
        Pass = new JLabel();
        Pass.setBounds(10, 50, 100, 20);
        Pass.setText("Password");
        contentPane.add(Pass);
        
        Password = new JPasswordField();
        Password.setBounds(10, 70, 150, 20);
        contentPane.add(Password);
        
        OK = new JButton();
        OK.setBounds(10, 100, 90, 20);
        OK.setText("OK");
        contentPane.add(OK);
        OK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                    boolean successful = engine.Authenticate(Username.getText(), Password.getText());
                    if (successful == true) {
                        setVisible(false);
                        MainInterface inter = new MainInterface(Username.getText(), engine);
                    }
            }
        });
        
        Registration = new JButton();
        Registration.setBounds(10, 140, 120, 20);
        Registration.setText("Registration");
        contentPane.add(Registration);
        Registration.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                Registration register = new Registration(engine);
            }
        });
        
        Cancel = new JButton();
        Cancel.setBounds(110, 100, 90, 20);
        Cancel.setText("Cancel");
        contentPane.add(Cancel);
        Cancel.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               System.exit(0);
           } 
        });
        
        setTitle("Login");
        setSize(250, 200);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        ClientSide application = new ClientSide();
    }
    
}
